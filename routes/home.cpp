#include "routes.h"
#include "../servehelper.h"

void home_route(const httplib::Request& req, httplib::Response& res, const Config& config) {
    std::string origin = get_origin(req, config);
    auto get_artwork_element = [&](size_t illust_id, const char* illust_title, size_t author_id, const char* author_display_name) {
        return Element("li", {
            Element("a", {{"href", origin + "/artworks/" + std::to_string(illust_id)}}, {illust_title}),
            " by ",
            Element("a", {{"href", origin + "/users/" + std::to_string(author_id)}}, {author_display_name}),
        });
    };

    Element body("body", {
        "Pixwhile is an alternative frontend to Pixiv that utilizes no Javascript. (",
        Element("a", {{"href", "https://gitlab.com/blankX/pixwhile"}}, {"source code"}),
        ")",
        Element("form", {{"method", "get"}, {"action", "search"}}, {
            Element("br"),
            Element("input", {{"name", "q"}, {"required", ""}}, {}),
            " ",
            Element("button", {"Search for illustrations"})
        }),
        Element("h2", {"Try it out"}),
        Element("ul", {
            get_artwork_element(106623268, "アル社長の日常", 1960050, "torino"),
            get_artwork_element(94449946, "Gura and Shion", 15961697, "ZWJ")
        }),
        Element("h2", {"Features"}),
        Element("ul", {
            Element("li", {"Does not contain an asinine amount of Javascript"}),
            Element("li", {"Allows you to open the original versions of cover images and profile pictures"}),
            Element("li", {"Can view illustrations and list user illustrations"}),
            Element("li", {"Can search for newest and oldest illustrations"})
        }),
        Element("h2", {"Missing Features"}),
        Element("p", {"This list is not exhaustive, nor does it mean that these are being worked on."}),
        Element("ul", {
            Element("li", {"Can only search for newest and oldest illustrations"}),
            Element("li", {"No ability to login"}),
            Element("li", {"No ability to see comments or like counts"}),
        }),
        Element("h2", {"Miscellaneous Information"}),
        Element("p", {
            "In a page containing a list of illustrations (e.g. a user's illustrations page or a search results page), "
            "illustrations containing multiple images will have a badge indicating the amount of images inside. "
            "Illustrations that are marked as being AI-generated will have a badge with a red background."
        })
    });
    serve(req, res, config, "Pixwhile", std::move(body));
}
