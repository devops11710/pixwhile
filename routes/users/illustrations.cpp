#include <utility>

#include "../routes.h"
#include "../../numberhelper.h"
#include "../../servehelper.h"
#include "../../pixivclient.h"
#include "common.h"

static inline Nodes generate_ogp_nodes(const httplib::Request& req, const Config& config, const User& user, uint64_t page);

void user_illustrations_route(const httplib::Request& req, httplib::Response& res, const Config& config, PixivClient& pixiv_client) {
    uint64_t user_id = to_ull(req.matches[1].str());
    uint64_t page = req.has_param("p") ? to_ull(req.get_param_value("p")) - 1 : 0;
    User user;
    Illusts illusts;

    try {
        user = pixiv_client.get_user(user_id);
        illusts = pixiv_client.get_illusts(user_id, page);
    } catch (const PixivException& e) {
        if (e.status == 404) {
            res.status = 404;
            serve_error(req, res, config, "404: User not found", e.what());
        } else {
            res.status = 500;
            serve_error(req, res, config, "500: Internal server error", "Failed to fetch user information", e.what());
        }
        return;
    } catch (const std::exception& e) {
        res.status = 500;
        serve_error(req, res, config, "500: Internal server error", "Failed to fetch user information", e.what());
        return;
    }

    Element body("body", {
        generate_user_header(std::move(user), req, config),
        generate_illusts_pager(req, config, illusts, page, "illusts")
    });
    serve(req, res, config, user.display_name + "'s illustrations", std::move(body), generate_ogp_nodes(req, config, user, page));
}

static inline Nodes generate_ogp_nodes(const httplib::Request& req, const Config& config, const User& user, uint64_t page) {
    using namespace std::string_literals;

    std::string url = get_origin(req, config) + "/users/" + std::to_string(user.user_id) + "/illustrations";
    if (page != 0) {
        url += "?p="s + std::to_string(page + 1);
    }
    Nodes nodes({
        Element("meta", {{"property", "og:title"}, {"content", user.display_name + " (@" + user.username + ')'}}, {}),
        Element("meta", {{"property", "og:type"}, {"content", user.ogp_image ? "photo" : "website"}}, {}),
        Element("meta", {{"property", "og:site_name"}, {"content", "Pixwhile"}}, {}),
        Element("meta", {{"property", "og:url"}, {"content", std::move(url)}}, {})
    });
    if (user.ogp_image) {
        nodes.push_back(Element("meta", {{"property", "og:image"}, {"content", proxy_image_url(req, config, *user.ogp_image)}}, {}));
    } else {
        const Image& image = user.profile_pictures.thumbnail_or_original();
        nodes.push_back(Element("meta", {{"property", "og:image"}, {"content", proxy_image_url(req, config, image.url)}}, {}));
        if (image.size) {
            nodes.push_back(Element("meta", {{"property", "og:image:width"}, {"content", std::to_string(image.size->first)}}, {}));
            nodes.push_back(Element("meta", {{"property", "og:image:height"}, {"content", std::to_string(image.size->second)}}, {}));
        }
    }

    return nodes;
}
