#include "common.h"
#include "../../config.h"
#include "../../servehelper.h"
#include "../../pixivmodels.h"

static inline Element generate_user_links(const User& user);

Element generate_user_header(const User& user, const httplib::Request& req, const Config& config) {
    Element header("header");
    if (user.cover_images) {
        std::string cover_original = proxy_image_url(req, config, user.cover_images->original_or_thumbnail().url);
        std::string cover_thumbnail = proxy_image_url(req, config, user.cover_images->thumbnail_or_original().url);
        header.nodes.push_back(Element("a", {{"href", std::move(cover_original)}}, {
            Element("img", {{"class", "user-cover"}, {"loading", "lazy"}, {"src", std::move(cover_thumbnail)}}, {})
        }));
    }

    std::string profile_picture_original = proxy_image_url(req, config, user.profile_pictures.original_or_thumbnail().url);
    std::string profile_picture_thumbnail = proxy_image_url(req, config, user.profile_pictures.thumbnail_or_original().url);
    header.nodes.push_back(Element("div", {{"class", "user_metadata"}}, {
        Element("a", {{"href", std::move(profile_picture_original)}}, {
            Element("img", {{"class", "user_profile_picture"}, {"loading", "lazy"}, {"src", std::move(profile_picture_thumbnail)}}, {})
        }),
        Element("div", {
            Element("p", {Element("b", {user.display_name}), " (@", user.username, ")"}),
            generate_user_links(user)
        })
    }));
    return header;
}

static inline Element generate_user_links(const User& user) {
    Element p("p");
    for (const auto &[name, url] : user.links) {
        if (!p.nodes.empty()) {
            p.nodes.push_back(", ");
        }
        p.nodes.push_back(Element("a", {{"href", url}}, {name}));
    }
    return p;
}
