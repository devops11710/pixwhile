#pragma once

#include <vector>
#include <httplib/httplib.h>
#include <nlohmann/json.hpp>

#include "pixivmodels.h"
class Redis; // forward declared from hiredis_wrapper.h

class PixivClient {
public:
    PixivClient(Redis* redis);

    User get_user(uint64_t user_id);
    Illusts get_illusts(uint64_t user_id, size_t page);
    Illust get_illust(uint64_t illust_id);

    SearchResults search_illusts(std::string query, size_t page, const std::string& order);
    std::vector<SearchSuggestion> get_search_suggestions(std::string query);

    bool i_pximg_url_valid(const std::string& path);

private:
    nlohmann::json _call_api(std::string cache_key, std::optional<std::string> cache_field, time_t expiry,
            std::string path, httplib::Params params, httplib::Headers headers);
    httplib::Client _www_pixiv_net_client{"https://www.pixiv.net"};
    httplib::Client _i_pximg_net_client{"https://i.pximg.net"};
    Redis* _redis;
};

class HTTPLibException : public std::exception {
public:
    HTTPLibException(httplib::Error error) {
        this->_message = httplib::to_string(error);
    }

    inline const char* what() const noexcept {
        return this->_message.c_str();
    }

private:
    std::string _message;
};

class PixivException : public std::exception {
public:
    PixivException(int status_, std::string message) : status(status_), _message(std::move(message)) {}

    inline const char* what() const noexcept {
        return this->_message.c_str();
    }

    int status;

private:
    std::string _message;
};
