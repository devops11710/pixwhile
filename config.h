#pragma once

#include <string>
#include <variant>
#include <nlohmann/json.hpp>
#include "blankie/murl.h"

struct IPConnection {
    std::string address;
    int port;
};
struct UnixConnection {
    std::string unix;
};

struct RedisConfig {
    std::variant<IPConnection, UnixConnection> connection_method;
    std::optional<std::string> username;
    std::optional<std::string> password;
};

struct Config {
    std::string bind_host = "127.0.0.1";
    int bind_port = 8080;
    blankie::murl::Url image_proxy_url{"https://i.pixiv.cat"};
    std::optional<std::string> canonical_origin;
    std::optional<RedisConfig> redis_config;
};

Config load_config(const char* path);
void from_json(const nlohmann::json& j, Config& config);
