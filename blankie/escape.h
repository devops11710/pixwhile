#pragma once

#include <string>

namespace blankie {
namespace html {

std::string escape(const std::string& in);

} // namespace html
} // namespace blankie
